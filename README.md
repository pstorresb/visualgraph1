El presente proyecto consiste en el desarrollo de [visualGraph](http://178.128.231.142:8000/visualGraph/), una herramienta Web de análisis y visualización de datos relacionales. 


La herramienta permite la conexión con una base de datos en grafo (alojada en [neo4j](https://neo4j.com/)), la generación de consultas basadas en caminos tipados, así como una visualización configurable de los resultados de las consultas.



Adicionalmente la aplicación genera archivos descargables con los datos resultado. Para representaciones visuales a través de la herramienta [Gephi](https://gephi.org/).



  ![ejemplo](https://gitlab.com/pstorresb/visualgraph1/raw/master/img/ejemplo.png)


# Implementación:

El proyecto implementa el framework de desarrollo web [Django](https://www.djangoproject.com/). Librerías como py2neo, networkx y sigmaJs.

## Software requerido:



- [python-3.7.3.](https://www.python.org/downloads/)




- [pycharm-community-2019.1.2](https://www.jetbrains.com/pycharm/download/)    (opcional)


## Ejecución:

Los pasos para ejecutar el proyecto mediante el IDE de python  [pycharm](https://www.jetbrains.com/pycharm/) se encunetran en el siguiente manual.. [DESCARGAR](https://gitlab.com/pstorresb/visualgraph1/raw/master/Manual%20de%20ejecuci%C3%B3n%20de%20c%C3%B3digo%20fuente.pdf)






## Licencias:

 [Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/legalcode)
  
  ![Creative commons BY SA](https://licensebuttons.net/l/by-sa/3.0/88x31.png)
  
  
  
   Pablo Torres