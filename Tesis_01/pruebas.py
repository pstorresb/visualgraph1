import networkx as nx
from py2neo import Graph
from py2neo.data import Node
import numpy as np
import math
import random


def propOfNodes(host,user,password, tipoNodo):
    propierties=[]
    graph = Graph(host, user=user, password=password)
    query='MATCH (p:'+tipoNodo+')  RETURN Distinct keys(p) limit 1'

    query = str(query)

    cursor = graph.run(query)
    auxDic={}

    for c in cursor:

        propierties= c[0]

    propierties.append('tipoNodo')

   # print(propierties)
    return propierties




def propOfNode(graph=Graph,idNode=0):

    #graph = Graph(host, user=user, password=pwd)
    query = "match (n) where id(n)="+str(idNode)+ " return n"
    dictProp={}
    query = str(query)

    cursor = graph.run(query)

    for c in cursor:
        dictProp=c[0]

    return dictProp







def traversalFunction2(host,user,password,query,nombre,limit,btwns,degree,pgRk,fnLogic):
    nodesGroup = {}
    tipoNodos = {}
    nodosProp = {}
    nodesAr = []
    nodesBr = []


    G = nx.Graph()


    auxLimit=int(limit)

    graph = Graph(host,user=user,password=password)


    query = str(query)

    if (str(fnLogic)=='0'):
        query= "match p="+query+" return distinct id(a), id(b),labels(a), labels(b),count(p) LIMIT " + str(auxLimit)
        print(query)
    else:
        query = "Match (a)-[]-(b) where" + query + "return a,id(a),b,id(b),labels(a), labels(b) LIMIT " + str(auxLimit)

        print(query)

    print(  'prueba0.........')
    mylist=[
        value for value in graph.run(query)
    ]

    print(str(len(mylist))+'prueba1')


    for element in mylist:
        auxDic = {}
        auxDic2 = {}
        idA=element[0]
        idB=element[1]
        labelA=element[2][0]
        labelB=element[3][0]
        weightRel=element[4]

        G.add_edge(idA, idB,weight=weightRel)

        attrsA=propOfNode(graph,idA)

        attrsB = propOfNode(graph, idB)
    #-----------------------------ADICION-DE-ATRIBUTOS--------------------------------------
        for prop in attrsA:
            G.node[idA][prop]=attrsA[prop]
            auxPal = str(attrsA[prop]).replace("'", "_")
            auxPal = auxPal.replace("  ", " ")
            auxPal = auxPal.replace("   ", " ")
            auxDic[prop] = auxPal

        nodesGroup[idA]=auxDic
        nodesGroup[idA]['tipoNodo']=labelA




        for prop in attrsB:
            G.node[idB][prop] = attrsB[prop]
            auxPal2 = str(attrsB[prop]).replace("'", "_")
            auxPal2 = auxPal2.replace("  ", " ")
            auxPal2 = auxPal2.replace("   ", " ")
            auxDic2[prop] = auxPal2

        nodesGroup[idB]=auxDic2
        nodesGroup[idB]['tipoNodo'] = labelB

        tipoNodos[labelA] = labelA
        tipoNodos[labelB] = labelB
        G.node[idA]['tipoNodo'] = str(labelA)
        G.node[idB]['tipoNodo'] = str(labelB)
    # -----------------------------FIN ADICION-DE-ATRIBUTOS--------------------------------------

    for node in tipoNodos:

        nodosProp[node]=propOfNodes(host,user,password,node)



    # **************metricas********************
    if (int(btwns) == 1):
        btnessNode = nx.betweenness_centrality(G)
        for n in btnessNode:
            G.node[n]['betweeness'] = btnessNode[n]
            nodesGroup[n]['betweeness'] = btnessNode[n]


    if (int(degree) == 1):
        degreeNode = nx.degree_centrality(G)
        for n in degreeNode:
            G.node[n]['degree'] = degreeNode[n]
            nodesGroup[n]['degree'] = degreeNode[n]

    if (int(pgRk) == 1):
        prankNode = nx.pagerank(G)
        for n in prankNode:
            G.node[n]['pagerank'] = prankNode[n]
            nodesGroup[n]['pagerank'] = prankNode[n]
    #nx.write_gexf(G, "c2_2016(autor).gexf")

    for n in nodesGroup:
        nodesGroup[n]['x'] = random.uniform(1, 6)
        nodesGroup[n]['y'] = random.uniform(1, 6)

        # EXPORTACION A GEPHI

        nx.write_gexf(G, "tesisForm/static/tesisForm/" + nombre + ".gexf")

        dict3 = {1: nodesGroup, 3: nodesAr, 4: nodesBr, 2: nodosProp}

        return dict3









#traver1='(a:Pais)<-[:tiene_un_pais_de_origen]-(:Autor)<-[:tiene_asociado_un_autor]-(:Exposicion)-[:tiene_asociado_un_autor]->(:Autor)-[:tiene_un_pais_de_origen]->(b:Pais) '

#traver2='(a:Pais)<-[:tiene_un_pais_de_origen]-(:Autor)<-[:tiene_asociado_un_autor]-(:Exposicion)-[:organizada_por_la_entidad]->(b:Entidad)'
#traver3='(a:Pais)<-[:tiene_un_pais_de_origen]-(:Autor)<-[:tiene_asociado_un_autor]-(:Exposicion)-[:organizada_por_la_entidad]->(b:Entidad)'

#traver13='(a:Pais)<-[:tiene_un_pais_de_origen]-(:Comisario)<-[:tiene_asociado_un_comisario]-(:Exposicion)-[:organizada_por_la_entidad]->(b:Entidad)'
#traver10=' (a:Pais)<-[:tiene_un_pais_de_origen]-(:Autor)<-[:tiene_asociado_un_autor]-(e:Exposicion)-[:tiene_asociado_un_autor]->(:Autor)-[:tiene_un_pais_de_origen]->(b:Pais) where (e)-[:llevada_a_cabo_en]->(:Año{numero:2016})'
#traver13=' (a:Pais)<-[:tiene_un_pais_de_origen]-(:Comisario)<-[:tiene_asociado_un_comisario]-(e:Exposicion)-[:organizada_por_la_entidad]->(b:Entidad) where (e)-[:llevada_a_cabo_en]->(:Año{numero:2016})'
traver12=' (a:Pais)<-[:tiene_un_pais_de_origen]-(:Autor)<-[:tiene_asociado_un_autor]-(e:Exposicion)-[:organizada_por_la_entidad]->(b:Entidad) where (e)-[:llevada_a_cabo_en]->(:Año{numero:2016})'

traversalFunction2('http://localhost:7474', 'neo4j', 'pstorres', traver12, 'c1_we', 9000000,1,1,1,0)


