from django import forms


class DBForm(forms.Form):
    nombreDB = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'class':'form-control', 'id':'nombreDB', 'name':'nombreDB', 'placeholder': 'Nombre de base de datos*'}))
    servidor=forms.CharField(max_length=100,required=True, widget=forms.TextInput(attrs={'class':'form-control','id':'servidor','name':'servidor','placeholder':'Host*'}))
    user = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'class':'form-control','id':'database','name':'user','placeholder':'User*'}))
    #password = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'pwd', 'name': 'pwd1', 'placeholder': 'Password*'}))
    pwd1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','id':'pwd','name':'pwd1','placeholder':'Password*'}))

