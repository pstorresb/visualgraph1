var originalFormu = $("#crearInterac").clone();



function addNodeRel() {

    //OBTENCION DEL ID DEL ULTIMO ELEMENTO CREADO Y ASIGNACION A VARIABLE PARA NUEVO ELEMTO
    //NODO
    var $lastNode = $('div[id^="node"]:last');
    var numNode = parseInt($lastNode.prop("id").match(/\d+/g), 10) + 1;


    //RELACION
    var $lastRelation = $('div[id^="relation"]:last');
    var numRelation = parseInt($lastRelation.prop("id").match(/\d+/g), 10) + 1;

    //DIRECCIONES
    var $lastDir = $('div[id^="dir"]:last');
    var numDir = parseInt($lastDir.prop("id").match(/\d+/g), 10) + 1;


    var $lastAux = $('div[id^="auxi"]:last');
    var numAuxi = parseInt($lastAux.prop("id").match(/\d+/g), 10) + 1;


    //CLONACION DE ELEMENTOS

    var $newNode = $lastNode.clone().prop('id', 'node' + numNode);
    var $newAux = $lastAux.clone().prop('id', 'auxi' + numNode);

    var $newRelation = $lastRelation.clone().prop('id', 'relation' + numRelation);

    var $newDir = $lastDir.clone().prop('id', 'dir' + numDir);


    $lastNode.after($newRelation);

    $newRelation.after($newNode);
    $lastAux.after($newDir);
    $newDir.after($newAux);
}



function deleteNodeRel() {
    //OBTENCION DEL ID DEL ULTIMO ELEMENTO CREADO Y ASIGNACION A VARIABLE PARA NUEVO ELEMTO
    //NODO
    var $lastNode = $('div[id^="node"]:last');
    var numNode = parseInt($lastNode.prop("id").match(/\d+/g), 10);


    //RELACION
    var $lastRelation = $('div[id^="relation"]:last');
    var numRelation = parseInt($lastRelation.prop("id").match(/\d+/g), 10);

    //DIRECCIONES
    var $lastDir = $('div[id^="dir"]:last');
    var numDir = parseInt($lastDir.prop("id").match(/\d+/g), 10);


    var $lastAux = $('div[id^="auxi"]:last');
    var numAuxi = parseInt($lastAux.prop("id").match(/\d+/g), 10);


    if (numNode > 2) {


        $('#node' + numNode).remove();
        $('#relation' + numRelation).remove();
        $('#dir' + numDir).remove();
        $('#auxi' + numAuxi).remove();
    }
}


function crearTraversal() {
    var aux;



    var nodos = [];
    var trav = [];
    var dir = [];
    var dirF = [];

    $(".nodos option:selected").each(function () {
        nodos.push($(this).text());

    })


    $(".traversals option:selected").each(function () {
        trav.push($(this).text());
    })

    $(".dir option:selected").each(function () {
        dir.push($(this).text());
    })
    for (var h = 0; h < dir.length; h++) {
        switch (dir[h]) {
            case '--->':
                dirF.push("-");
                dirF.push("->");
                break;

            case '<---':
                dirF.push("<-");
                dirF.push("-");
                break;

            case '----':
                dirF.push("-");
                dirF.push("-");
                break;

        }




    }


    dir = dirF;

    var j;
    for (j = 0; j < nodos.length; j++) {
        if (nodos[j] == "any!") {
            nodos[j] = ""
        } else {
            nodos[j] = ":" + nodos[j]
        }
    }
    if (nodos.length > 2) {
        var query = "";
        var i;
        var aux = 1;
        //cuestion el -1
        for (i = 0; i < nodos.length - 1; i++) {

            if (i == 0) {
                query = "(a" + nodos[0] + ")" + dir[0] + "[:" + trav[0] + "]" + dir[1] + "(" + nodos[1] + ")"

            }
            else if (i == nodos.length - 2) {
                query = query + dir[i + aux] + "[:" + trav[i] + "]" + dir[i + 1 + aux] + "(b" + nodos[i + 1] + ")"
                aux = aux + 1;

            }
            else {

                query = query + dir[i + aux] + "[:" + trav[i] + "]" + dir[i + 1 + aux] + "(" + nodos[i + 1] + ")";

                aux = aux + 1;

            }
        }

    } else {
        var query = "(a" + nodos[0] + ")" + dir[0] + "[:" + trav[0] + "]" + dir[1] + "(b" + nodos[1] + ")";


    }

    var nodeA = nodos[0];
    var nodeB = nodos[nodos.length - 1];
    var indexT = $("#listaPro li").length + 1;

    $('#traversal').val(query);
    $("#listaPro").append("<li class='free listaTra' id='travelList" + indexT + "'>" + query + "</li>");
    $("#listaProAux").append("<li class='free list-group-item' id='travelListo" + indexT + "'>" + "T" + indexT + "= " + query + "</li>");

    if ($("#listaPro li").length >= 2) {
        $("#funcLogic").show();
    } else {
        $("#funcLogic").hide();
    }


    if ($("#listaPro li").length >= 1) {
        $("#ListAddTraversal").show();
    } else {
        $("#ListAddTraversal").hide();
    }


    $('#funcionLogica').val('0');


}


function eliminarTraversal() {

    var indexT = $("#listaPro li").length;


    $("#travelList" + indexT).remove();
    $("#travelListo" + indexT).remove();

    var oldElement = $(".listaTra").eq(indexT - 2).text();
    $('#traversal').val(oldElement);


    if ($("#listaPro li").length >= 2) {
        $("#funcLogic").show();
    } else {
        $("#funcLogic").hide();
    }
    if ($("#listaPro li").length >= 1) {
        $("#ListAddTraversal").show();
    } else {
        $("#ListAddTraversal").hide();
    }







}

function addCam() {

    indexCamino = $("#camino").val();
    var separ = indexCamino.split("");
    for (var j = 0; j < separ.length; j++) {
        if (separ[j] == "T") {
            indexAux = (parseInt(separ[j + 1], 10))
            var caminoLista = $(".listaTra").eq(indexAux - 1).text();
            indexCamino = indexCamino.replace(separ[j] + separ[j + 1], caminoLista);
        }
    }
    $('#traversal').val(indexCamino);

    $('#funcionLogica').val('1');
};









function checkMetric() {
    if ($('#met1').is(":checked")) {

        $('#metric1').val('1');
        // it is checked
    }
    else {
        $('#metric1').val('0');
    }


    if ($('#met2').is(":checked")) {

        $('#metric2').val('1');
        // it is checked
    }
    else {
        $('#metric2').val('0');
    }


    if ($('#met3').is(":checked")) {

        $('#metric3').val('1');
        // it is checked
    }
    else {
        $('#metric3').val('0');
    }

};




function showLoada() {



    $('.recibir-imagen').attr('src', 'https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif');
    $('#mimodal').modal();


};





 var dNOrigen = '{{dNOrigen|safe}}';
        var nOrigen = '{{nOrigen|safe}}';
        var nDestino = '{{nDestino|safe}}';
        var nodosProp = '{{nodosProp|safe}}';
        var i,
            g = {
                nodes: [],
                edges: []
            };

        var dNOrigenJs = JSON.parse(dNOrigen);
        var nOrigenJs = JSON.parse(nOrigen);
        var nDestinoJS = JSON.parse(nDestino);
        var nodosPropJS = JSON.parse(nodosProp);
        var relCount = Object.keys(nOrigenJs).length;
        //console.log(relCount)

        var numProp = Object.keys(nodosPropJS).length;

        for (var key in dNOrigenJs) {
            var sm = {
                id: key,
                label: '',
                size: Math.random(),
                color: '#fec20f'

            }

            for (var key2 in dNOrigenJs[key]) {
                var swe3 = String(dNOrigenJs[key][key2])
                sm[key2] = swe3
            }

            g.nodes.push(sm);

        }




        for (i = 0; i < relCount; i++)
            g.edges.push({
                id: 'e' + i,
                label: 'T',
                source: nOrigenJs[i],
                target: nDestinoJS[i],
                size: 2,
                color: '#' + (
                    Math.floor(Math.random() * 16777215).toString(16) + '000000'
                ).substr(0, 6),
                type: 'arrow',
                count: i
            });



        s = new sigma({
            graph: g,
            renderer: {
                container: document.getElementById('graph-container'),
                type: 'webGL'
            },
            settings: {
                defaultLabelColor: '#eeedd9',
                defaultLabelSize: 16,
                defaultEdgeLabelColor: '#c2b4b4',
                minNodeSize: 8,
                minEdgeSize: 0.7,
                maxEdgeSize: 3.5,
                maxNodeSize: 16

            }
        });


        var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);

        dragListener.bind('startdrag', function (event) {
        });
        dragListener.bind('drag', function (event) {
        });
        dragListener.bind('drop', function (event) {
        });
        dragListener.bind('dragend', function (event) {

        });

        function crearListasTip() {
            var nodosProp = '{{nodosProp|safe}}';
            var nodosPropJS = JSON.parse(nodosProp);
            for (var e in nodosPropJS) {
                $('#propiedades').append('<div class="col-lg-12">  <h5 class="text-uppercase col textPeke tipoNodo">' + e + '</h5>');
                var sel = $('<div class="col-lg-12 nodos"> <select class="form-control propNodes nodos" id="' + e + '">').appendTo('#propiedades');
                for (var e1 in nodosPropJS[e]) {
                    $('#' + e).append($("<option>").text(nodosPropJS[e][e1]));
                }
            }

        }

        crearListasTip();

        function apliForceP() {
            var tiposNodos = []
            var propSelect = []


            $('.tipoNodo').each(function () {
                tiposNodos.push($(this).text());
                //console.log(tiposNodos)

            })

            $(".propNodes option:selected").each(function () {

                propSelect.push($(this).text());

            });

            for (var h = 0; h < propSelect.length; h++) {
                var auxProp = propSelect[h];
                var auxLabel = tiposNodos[h];
                s.graph.nodes().forEach(function (n) {
                    if (n.tipoNodo == auxLabel) {
                        n.label = n[auxProp];


                    }
                });

            }


            s.refresh();
        }

        function apliForce() {

            s.startForceAtlas2({ worker: true, barnesHutOptimize: true, ScalingRatio:7, outboundAttractionDistribution:true});


            s.refresh();
        }

        function apliForceStop() {

            s.stopForceAtlas2();

            s.refresh();
        }

        function noverlaaap() {
            var noverlapListener = s.configNoverlap({
                nodeMargin: 15,
                scaleNodes: 0.5,
                gridSize: 75,
                easing: 'quadraticInOut',
                duration: 1000
            });
            // Bind the events:
            noverlapListener.bind('start stop interpolate', function (e) {
                //console.log(e.type);
                if (e.type === 'start') {
                   // console.time('noverlap');
                }
                if (e.type === 'interpolate') {
                    //console.timeEnd('noverlap');
                }
            });
            // Start the layout:
            s.startNoverlap();

            s.refresh();
        }

        Array.prototype.unique = function (a) {
            return function () { return this.filter(a) }
        }(function (a, b, c) {
            return c.indexOf(a, b + 1) < 0
        });


        function dibujarPropiedad(prop) {
            var dNOrigen = '{{dNOrigen|safe}}';
            var dNOrigenJs = JSON.parse(dNOrigen);
            var nodesProp = [];

            var arrayt = {
                values1: [],
                colors: [],
                sizes: []

            };
            var nodesProp = [];
            for (var key in dNOrigenJs) {
                var sm = {}
                //console.log(sm['size'])

                for (var key2 in dNOrigenJs[key]) {

                    var swe3 = String(dNOrigenJs[key][key2])
                    sm[key2] = swe3

                }
                var valueProp = sm[prop]

                nodesProp.push(valueProp)

            }
            nodesProp = nodesProp.unique();
            nodesProp.sort();
            //console.log(nodesProp)
            for (var j = 0; j < nodesProp.length; j++) {
                var colorAux = '#' + (Math.floor(Math.random() * 16777215).toString(16) + '000000').substr(0, 6);
                arrayt.values1.push(nodesProp[j]);
                arrayt.colors.push(colorAux);
                arrayt.sizes.push(0.6 + j);
            }
            for (var h = 0; h < arrayt.values1.length; h++) {
                var valueAux = arrayt.values1[h];
                var colorAux = arrayt.colors[h];
                var sizesAux = arrayt.sizes[h];

                s.graph.nodes().forEach(function (n) {

                    if (valueAux == n[prop]) {
                        n.color = colorAux;
                        n.size = sizesAux;
                    }
                });

            }
            s.refresh();
        }


        function forceAtlasSw() {
            var checkBox = document.getElementById("myCheck");

            var text = document.getElementById("text");

            if (checkBox.checked == true) {
                apliForce();
            } else {
                apliForceStop();
            }
        };

        $('input[id="dgr"]').on("click", function (e) {

            dibujarPropiedad('degree');
        })
        $('input[id="btns"]').on("click", function (e) {

            dibujarPropiedad('betweeness');
        })
        $('input[id="pgrk"]').on("click", function (e) {

            dibujarPropiedad('pagerank');
        })







