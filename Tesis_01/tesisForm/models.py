import datetime

from django.db import models
from django.conf import settings





class usuarioBaseF(models.Model):
    nombreDB = models.CharField(max_length=250)
    host = models.CharField(max_length=250)
    user = models.CharField(max_length=250)
    password = models.CharField(max_length=250)
    userConect=models.ForeignKey(settings.AUTH_USER_MODEL,default=1,on_delete=models.CASCADE)

    def __str__(self):
       return '%s %s %s' % (self.nombreDB,self.host, self.userConect)


class traversalsUSer(models.Model):
    traversal = models.CharField(max_length=550)
    baseConect=models.ForeignKey(usuarioBaseF,on_delete=models.CASCADE)
    userConect=models.ForeignKey(settings.AUTH_USER_MODEL,default=1,on_delete=models.CASCADE)
    def __str__(self):
        return '%s %s %s' % (self.traversal,self.baseConect, self.userConect)





