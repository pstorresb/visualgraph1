from django.apps import AppConfig


class TesisformConfig(AppConfig):
    name = 'tesisForm'
