

from . import views
from django.urls import path
app_name='tesisForm'
urlpatterns = [

    path('', views.presentacion, name='presentacion'),
    path('ingresar/', views.autenticacion, name='autenticacion'),
    path('server/', views.defConsulta, name='desarrollo'),
    path('visualizacion/', views.visualizacion, name='visualizacion'),
    path('prueba1/', views.eliminarDB, name='prueba1'),
    path('prueba2/', views.eliminarConsulta, name='prueba2'),
    path('signUp/', views.SignUp.as_view(), name='signUp'),

]