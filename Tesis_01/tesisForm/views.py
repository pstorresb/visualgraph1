from django.shortcuts import render, redirect
import json
from proyeccionesM import namesOfNodes,namesOfRelationships,graphSchema,traversalFunction2
from django.http import HttpResponse
from .models import usuarioBaseF,traversalsUSer
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from tesisForm.form import DBForm


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'tesisForm/signup.html'


#****************************************************************************************************************

def presentacion(request):
    authyu=request.user.is_authenticated
    if request.user.is_authenticated:
        return redirect("ingresar/")
    else:
        return render(request, 'tesisForm/presentacion.html')




@login_required()
def autenticacion(request):
    form= DBForm(request.POST)


    DBusuario = usuarioBaseF.objects.filter(userConect=request.user)

    return render(request,'tesisForm/autenticacion.html',{'form2':form,'datosUser': DBusuario})




@login_required()
def defConsulta(request):

    usuarioExiste = usuarioBaseF.objects.filter(host=request.POST['servidor'], userConect=request.user,nombreDB=request.POST['nombreDB']).exists()

    baseNuev = usuarioBaseF(host=request.POST['servidor'], user=request.POST['user'],password=request.POST['pwd1'], userConect=request.user, nombreDB=request.POST['nombreDB'])

    if usuarioExiste==False:
        print('hello')
        baseNuev.save()

    server = request.POST['servidor']
    user= request.POST['user']
    pwd=  request.POST['pwd1']
    request.session['baseNeoUser']=user
    request.session['baseNeoServer']=server
    request.session['baseNeoPwd'] = pwd


    nombreBase= request.POST['nombreDB']
    node_list=namesOfNodes(server,user,pwd)
    relation_list = namesOfRelationships(server, user, pwd)

    schemaG= graphSchema(server,user,pwd)
    schemaG1=json.dumps(schemaG[1])
    schemaG2 = json.dumps(schemaG[2])
    schemaG3 = json.dumps(schemaG[3])
    node_listSq=json.dumps(node_list)
    baseActiva1 = usuarioBaseF.objects.filter(userConect=request.user,nombreDB=nombreBase,host=str(server).strip()).first()
    traversalsAlmace = traversalsUSer.objects.filter(userConect=request.user,baseConect=baseActiva1)

    return render(request, 'tesisForm/desarrollo.html',{'node_list':node_list,'relation_list':relation_list,'schemaG1':schemaG1, 'schemaG2':schemaG2, 'schemaG3':schemaG3, 'node_listSq':node_listSq,'traveUser':traversalsAlmace})



@login_required()
def visualizacion(request):

    neoUser=request.session.get('baseNeoUser')
    neoServer = request.session.get('baseNeoServer')
    neoPwd = request.session.get('baseNeoPwd')
    #print(neoUser,neoServer,neoPwd)

    traversal = request.POST['traversal']

    baseActiva1 = usuarioBaseF.objects.filter(userConect=request.user,host=str(neoServer).strip()).first()


    travExiste = traversalsUSer.objects.filter(traversal=request.POST['traversal'], userConect=request.user,baseConect=baseActiva1).exists()

    travNuev = traversalsUSer(traversal=request.POST['traversal'], baseConect=baseActiva1, userConect=request.user)

    if travExiste==False:
        travNuev.save()

    nombreA=request.POST['nombreA']
    limit= request.POST['limit1']
    btwns = request.POST['btwns']
    degree = request.POST['degree']
    pgRk = request.POST['pgRk']

    fnLogic = request.POST['esFormula']

    datosSigma = traversalFunction2(neoServer, neoUser, neoPwd, traversal, nombreA, limit,btwns,degree,pgRk,fnLogic)

    dNOrigen=json.dumps(datosSigma[1])
    nOrigen = json.dumps(datosSigma[3])
    nDestino = json.dumps(datosSigma[4])
    nodosProp = json.dumps(datosSigma[2])

    #print(nDestino)


    return render(request, 'tesisForm/visualizacion.html',{'nombreA':nombreA,'dNOrigen':dNOrigen,'nOrigen':nOrigen,'nDestino':nDestino, 'nodosProp':nodosProp})

def eliminarDB(request):
    nombreDB=request.POST['nombreDBT']
    host=request.POST['hostT']

    baseActiva1 = usuarioBaseF.objects.filter(userConect=request.user, host=str(host).strip(),nombreDB=nombreDB).delete()
    return HttpResponse("OK")



def eliminarConsulta(request):
    traversalAlm = request.POST['travAlm']
    traversalX = traversalsUSer.objects.filter(userConect=request.user,traversal=traversalAlm).delete()

    return HttpResponse("OK")





