import networkx as nx
from py2neo import Graph
import math
import random




"""**************************************************************************************************************************************************"""

def namesOfNodes(host,user,password):

    graph=Graph(host,user=user,password=password)
    query2 = "MATCH (n) WITH DISTINCT LABELS(n) AS temp UNWIND temp AS label RETURN distinct label"
    cursor2 = graph.run(query2)
    nombresNodos = []
    for c in cursor2:
        nameNode = c[0]
        nombresNodos.append(nameNode)
    nombresNodos.sort()
    return nombresNodos

"""**************************************************************************************************************************************************"""


def namesOfRelationships(host,user,password):

    graph=Graph(host,user=user,password=password)
    query2 = "MATCH (a)-[r]->(b) RETURN DISTINCT TYPE(r)"
    cursor2 = graph.run(query2)
    nombresRelaciones = []
    for c in cursor2:
        nameNode = c[0]
        nombresRelaciones.append(nameNode)
    nombresRelaciones.sort()
    return nombresRelaciones






def graphSchema(host,user,password):
    nodesA=[]
    nodesB = []
    relationsG = []

    graphSch=nx.Graph()
    graph=Graph(host,user=user,password=password)
    query2 = "MATCH (a)-[r]->(b) RETURN DISTINCT Labels(a),Labels(b),type(r)"
    cursor2 = graph.run(query2)


    for c in cursor2:
       #print(c[0][0])
       ns1=c[0][0]
       ns2 = str(c[1][0])
       nrel= c[2]
       #print(ns1,ns2)
       nodesA.append(ns1)
       nodesB.append(ns2)
       relationsG.append(nrel)
       graphSch.add_edge(ns1,ns2, label=nrel)
       #numNodes = nx.number_of_nodes(GraphSchema())
       #auxK = (1 / math.sqrt(int(numNodes) * 2)) + 0.18


    diction={1:nodesA,2:nodesB,3:relationsG}

    return diction

#***************************************************************************************************************








def propOfNodes(host,user,password, tipoNodo):
    propierties=[]
    graph = Graph(host, user=user, password=password)
    query='MATCH (p:'+tipoNodo+')  RETURN Distinct keys(p) limit 1'

    query = str(query)

    cursor = graph.run(query)
    auxDic={}

    for c in cursor:

        propierties= c[0]

    propierties.append('tipoNodo')

   # print(propierties)
    return propierties





def traversalFunction(host,user,password,query,nombre,limit,btwns,degree,pgRk,fnLogic):
    #print(str(fnLogic))
    nodesGroup={}
    tipoNodos = {}
    nodosProp={}
    nodesAr=[]
    nodesBr=[]
    G = nx.Graph()


    auxLimit=int(limit)
    #print(auxLimit)


    graph = Graph(host,user=user,password=password)


    query = str(query)

    if (str(fnLogic)=='0'):
        query= "match p="+query+" return a, id(a), b, id(b), labels(a), labels(b) LIMIT "+str(auxLimit)
        #print(query)
        funLogig=False
    else:
        query = "Match (a)-[]-(b) where" + query + "return a,id(a),b,id(b),labels(a), labels(b) LIMIT " + str(auxLimit)

        #print(query)
        funLogig = True

    cursor= graph.run(query)

    for c in cursor:

        auxDic = {}
        auxDic2 = {}
        idNodeA=c[1]
        idNodeB=c[3]
        nombA=(c[4][0])
        nombB=(c[5][0])
        #print(nombA, nombB)

        nodesAr.append(c[1])
        nodesBr.append(c[3])


        #************-----------------------------NETWORKX----------------------*************************************

        if (funLogig == False):
            G.add_edge(idNodeA, idNodeB)
        else:
            G.add_edge(idNodeA, idNodeB)

        G.node[idNodeA]['tipoNodo'] = nombA

        G.node[idNodeB]['tipoNodo'] = nombB

#*****************************----ADICION DE propiedades en nodos:**************
        for x in c[0]:

           G.node[idNodeA][x] = c[0][x]
           auxPal=str(c[0][x]).replace("'", "_")
           auxPal=auxPal.replace("  "," ")
           auxPal = auxPal.replace("   ", " ")
           auxDic[x]=auxPal

        nodesGroup[idNodeA] = auxDic
        nodesGroup[idNodeA]['tipoNodo']=nombA

        for x in c[2]:
            G.node[idNodeB][x] = c[2][x]
            auxPal2 = str(c[2][x]).replace("'", "_")
            auxPal2 = auxPal2.replace("  ", " ")
            auxPal2 = auxPal2.replace("   ", " ")
            auxDic2[x] = auxPal2

        nodesGroup[idNodeB] = auxDic2
        nodesGroup[idNodeB]['tipoNodo'] = nombB


        tipoNodos[nombA] = nombA
        tipoNodos[nombB] = nombB




    for node in tipoNodos:

        nodosProp[node]=propOfNodes(host,user,password,node)


    # **************metricas********************
    if (int(btwns) == 1):
        btnessNode = nx.betweenness_centrality(G)
        for n in btnessNode:
            G.node[n]['betweeness'] = btnessNode[n]
            nodesGroup[n]['betweeness'] = btnessNode[n]


    if (int(degree) == 1):
        degreeNode = nx.degree_centrality(G)
        for n in degreeNode:
            G.node[n]['degree'] = degreeNode[n]
            nodesGroup[n]['degree'] = degreeNode[n]

    if (int(pgRk) == 1):
        prankNode = nx.pagerank(G)
        for n in prankNode:
            G.node[n]['pagerank'] = prankNode[n]
            nodesGroup[n]['pagerank'] = prankNode[n]







    #pos = nx.spring_layout(G,k=auxK)


    for n in nodesGroup:

        nodesGroup[n]['x'] = random.uniform(1,6)
        nodesGroup[n]['y'] = random.uniform(1,6)

    # EXPORTACION A GEPHI

    nx.write_gexf(G, "tesisForm/static/tesisForm/" + nombre + ".gexf")


    dict3 = {1: nodesGroup, 3: nodesAr, 4: nodesBr, 2: nodosProp}

    return dict3






