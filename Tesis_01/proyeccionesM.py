import networkx as nx
from py2neo import Graph
import random




"""**************************************************************************************************************************************************"""

def namesOfNodes(host,user,password):

    graph=Graph(host,user=user,password=password)
    query2 = "MATCH (n) WITH DISTINCT LABELS(n) AS temp UNWIND temp AS label RETURN distinct label"
    cursor2 = graph.run(query2)
    nombresNodos = []
    for c in cursor2:
        nameNode = c[0]
        nombresNodos.append(nameNode)
    nombresNodos.sort()
    return nombresNodos

"""**************************************************************************************************************************************************"""


def namesOfRelationships(host,user,password):

    graph=Graph(host,user=user,password=password)
    query2 = "MATCH (a)-[r]->(b) RETURN DISTINCT TYPE(r)"
    cursor2 = graph.run(query2)
    nombresRelaciones = []
    for c in cursor2:
        nameNode = c[0]
        nombresRelaciones.append(nameNode)
    nombresRelaciones.sort()
    return nombresRelaciones






def graphSchema(host,user,password):
    nodesA=[]
    nodesB = []
    relationsG = []

    graphSch=nx.Graph()
    graph=Graph(host,user=user,password=password)
    query2 = "MATCH (a)-[r]->(b) RETURN DISTINCT Labels(a),Labels(b),type(r)"
    cursor2 = graph.run(query2)


    for c in cursor2:
       #print(c[0][0])
       ns1=c[0][0]
       ns2 = str(c[1][0])
       nrel= c[2]
       #print(ns1,ns2)
       nodesA.append(ns1)
       nodesB.append(ns2)
       relationsG.append(nrel)
       graphSch.add_edge(ns1,ns2, label=nrel)

    diction={1:nodesA,2:nodesB,3:relationsG}

    return diction

#***************************************************************************************************************








def propOfNodes(host,user,password, tipoNodo):
    propierties=[]
    graph = Graph(host, user=user, password=password)
    query='MATCH (p:'+tipoNodo+')  RETURN Distinct keys(p) limit 1'

    query = str(query)

    cursor = graph.run(query)
    auxDic={}

    for c in cursor:

        propierties= c[0]

    propierties.append('tipoNodo')
    propierties.append('none')

   # print(propierties)
    return propierties

def propOfNode(graph=Graph,idNode=0):

    #graph = Graph(host, user=user, password=pwd)
    query = "match (n) where id(n)="+str(idNode)+ " return n"
    dictProp={}
    query = str(query)

    cursor = graph.run(query)

    for c in cursor:
        dictProp=c[0]

    return dictProp




def traversalFunction2(host,user,password,query,nombre,limit,btwns,degree,pgRk,fnLogic):
    nodesGroup = {}
    tipoNodos = {}
    nodosProp = {}
    nodesAr = []
    nodesBr = []
    G = nx.Graph()


    auxLimit=int(limit)

    graph = Graph(host,user=user,password=password)


    query = str(query)

    if (str(fnLogic)=='0'):
        query= "match p="+query+" return distinct id(a), id(b),labels(a), labels(b),count(p) LIMIT " + str(auxLimit)
        print(query)
    else:
        query = "Match (a),(b) where" + query + " return distinct id(a), id(b),labels(a), labels(b) LIMIT " + str(auxLimit)

        print(query)

    #print(  'prueba0.........')
    mylist=[
        value for value in graph.run(query)
    ]

    print(str(len(mylist))+' coincidencias')


    for element in mylist:
        auxDic = {}
        auxDic2 = {}
        idA=element[0]
        idB=element[1]
        labelA=element[2][0]
        labelB=element[3][0]

        nodesAr.append(idA)
        nodesBr.append(idB)
        if fnLogic=='1':
            G.add_edge(idA, idB)
        else:
            weightRel = element[4]
            G.add_edge(idA, idB, weight=weightRel)

       # print(' cont0')
        attrsA=propOfNode(graph,idA)

        attrsB = propOfNode(graph, idB)
       # print(' cont1')
    #-----------------------------ADICION-DE-ATRIBUTOS--------------------------------------
        for prop in attrsA:
            G.node[idA][prop]=attrsA[prop]
            auxPal = str(attrsA[prop]).replace("'", "_")
            auxPal = auxPal.replace("  ", " ")
            auxPal = auxPal.replace("   ", " ")
            auxDic[prop] = auxPal

        nodesGroup[idA]=auxDic
        nodesGroup[idA]['tipoNodo']=labelA


      #  print(' cont2')


        for prop in attrsB:
            G.node[idB][prop] = attrsB[prop]
            auxPal2 = str(attrsB[prop]).replace("'", "_")
            auxPal2 = auxPal2.replace("  ", " ")
            auxPal2 = auxPal2.replace("   ", " ")
            auxDic2[prop] = auxPal2

        nodesGroup[idB]=auxDic2
        nodesGroup[idB]['tipoNodo'] = labelB

        tipoNodos[labelA] = labelA
        tipoNodos[labelB] = labelB
        G.node[idA]['tipoNodo'] = str(labelA)
        G.node[idB]['tipoNodo'] = str(labelB)
    # -----------------------------FIN ADICION-DE-ATRIBUTOS--------------------------------------
  #  print(' cont3')
    for node in tipoNodos:

        nodosProp[node]=propOfNodes(host,user,password,node)



    # **************metricas********************
    if (int(btwns) == 1):
        btnessNode = nx.betweenness_centrality(G)
        for n in btnessNode:
            G.node[n]['betweeness'] = btnessNode[n]
            nodesGroup[n]['betweeness'] = btnessNode[n]


    if (int(degree) == 1):
        degreeNode = nx.degree_centrality(G)
        for n in degreeNode:
            G.node[n]['degree'] = degreeNode[n]
            nodesGroup[n]['degree'] = degreeNode[n]

    if (int(pgRk) == 1):
        prankNode = nx.pagerank(G)
        for n in prankNode:
            G.node[n]['pagerank'] = prankNode[n]
            nodesGroup[n]['pagerank'] = prankNode[n]

    #pos = nx.nx_agraph.graphviz_layout(G)
    for n in nodesGroup:
        nodesGroup[n]['x'] = random.uniform(1, 6)
        nodesGroup[n]['y'] = random.uniform(1, 6)


        # EXPORTACION A GEPHI

    nx.write_gexf(G, "tesisForm/static/tesisForm/" + nombre + ".gexf")

    dict3 = {1: nodesGroup, 3: nodesAr, 4: nodesBr, 2: nodosProp}
    #print(dict3)

    return dict3



